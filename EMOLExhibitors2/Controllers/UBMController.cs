﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EMOLExhibitors2.ViewModels;

namespace EMOLExhibitors2.Controllers
{
    public class UBMController : Controller
    {
        [HttpGet]
        [Authorize]
        public ActionResult Index()
        {
            var model = CrateIndexViewModel();
            return View(model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Index(FormCollection formCollection)
        {
            string fileName = formCollection["SelectedFile"] + ".csv";
            ViewBag.SelectedFile = formCollection["SelectedFile"];
            Session["SelectedFile"] = formCollection["SelectedFile"];
            
            var model = CrateIndexViewModel(ViewBag.SelectedFile);
            return View(model);
        }

        [Authorize]
        public ActionResult DownloadCSV()
        {
            string fileName = Session["SelectedFile"] + ".csv";
            ViewBag.SelectedFile = Session["SelectedFile"];

            var model = CrateIndexViewModel(ViewBag.SelectedFile);

            var reader = FTPFileDowbloadRequest(fileName);

            var current = System.Web.HttpContext.Current;
            current.Response.Clear();

            current.Response.ContentType = "text/csv";
            current.Response.ContentEncoding = System.Text.Encoding.Unicode;
            current.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
            
            string line = String.Empty;
            
            while ((line = reader.ReadLine()) != null)
            {
                current.Response.Write(line);
                current.Response.Write(Environment.NewLine);
            }

            current.Response.End();
            
            return View("Index", model);
        }

        private UBMIndexViewModel CrateIndexViewModel(string selectedFile = "")
        {
            var model = new UBMIndexViewModel();

            model.Files = GetListOfUBMFiles();
            model.SelectedFile = selectedFile;

            if (selectedFile != String.Empty) 
                model.Data = GetFileData(selectedFile);

            return model;
        }

        private List<UBMFile> GetListOfUBMFiles()
        {
            var reader = FTPListDirectoryRequest();
            
            string line = reader.ReadLine();
            
            string year = DateTime.Now.Year.ToString();
            string shortYear = year.Substring(2);
            
            var listOfFiles = new List<UBMFile>();

            while (!string.IsNullOrEmpty(line))
            {
                string itemCandidate = String.Empty;

                /*
                 * Directory contains booked_stands and unbooked_stands.
                 * Make sure that unbooked stands are excluded from the list. 
                 */

                if (line.IndexOf("booked_stands") != -1 && line.IndexOf("unbooked_stands") == -1)
                    itemCandidate = line.Substring(line.IndexOf("booked_stands"),
                                                   line.Length - line.IndexOf("booked_stands"));

                //if (line.IndexOf("Stand_Geometry") != -1)
                //    itemCandidate = line.Substring(line.IndexOf("Stand_Geometry"),
                //                                   line.Length - line.IndexOf("Stand_Geometry"));

                if (!itemCandidate.Contains("OLD") &&
                    (itemCandidate.Contains(year) || itemCandidate.Contains(shortYear)))
                {
                    var file = new UBMFile();
                    file.Name = itemCandidate.Substring(0, itemCandidate.Length - 4);
                    listOfFiles.Add(file);
                }
                line = reader.ReadLine();
            }

            reader.Close();

            return listOfFiles;
        }

        private List<DataRow> GetFileData(string selectedFile)
        {
            var fileName = selectedFile + ".csv";
            var reader = FTPFileDowbloadRequest(fileName);
            var line = reader.ReadLine();

            var dataTable = new DataTable();
            List<DataRow> rowList = new List<DataRow>();
            string[] headers = new string[]
                {
                    "ID No", "Exhbitor Name", "Exhibitor Username",
                    "Exhibitor Password", "Exhibting Name", "Company Name", "Stand Number", "Hall Name",
                    "Event Code", "X Dimension", "Y Dimension", "Ground Floor Area", "Net Area",
                    "Product Type Name", "Event Product", "Contract Status", "Admin Contact Title",
                    "First Name", "Last Name", "Address 1", "Address 2", "Address 3", "Town", "County",
                    "Postcode", "Country", "Phone", "Fax", "email", "Web Contact Title", "First Name",
                    "Last Name", "Address 1", "Address 2", "Address 3", "Town", "County", "Postcode",
                    "Country", "Phone", "Fax", "email", "Company Web", "Company Email", "web username",
                    "password", "Exhibitor Type", "Stand ID No", "Exhibitor ID No", "Company Phone",
                    "Can Use Co Phone", "Admin Contact ID", "Web Contact ID"
                };
           

            if (line != null)
            {
                string[] columns = line.Split('"')
                                       .Select((element, index) => index%2 == 0 // If even index
                                                                       ? element.Split(new[] {','}) // Split the item
                                                                       : new string[] {element}) // Keep the entire item
                                       .SelectMany(element => element).ToArray();

                for (int i = 0; i < columns.Length; i++)
                {
                    dataTable.Columns.Add("", typeof (string));
                }
                
                

                while (line != null)
                {
                    var row = dataTable.NewRow();
                    
                    var fields = line.Split('"')
                                     .Select((element, index) => index%2 == 0 // If even index
                                                                     ? element.Split(new[] {','}) // Split the item
                                                                     : new string[] {element}) // Keep the entire item
                                     .SelectMany(element => element).ToList();

                    for (int j = 0; j < fields.Count - 1; j++) row[j] = fields[j];
                    
                    rowList.Add(row);
                    line = reader.ReadLine();
                }
                reader.Close();

                foreach (var column in dataTable.Columns.Cast<DataColumn>().ToArray())
                {
                    var toBeDeleted = true;

                    foreach (var row in rowList)
                    {
                        if (!String.IsNullOrEmpty(row[column].ToString()))
                        {
                            toBeDeleted = false;
                            break;
                        }
                    }
                    if (toBeDeleted) dataTable.Columns.Remove(column);
                }

                var headerRow = dataTable.NewRow();
                for (int i = 0; i < headers.Count(); i++)
                {
                    try
                    {
                        headerRow[i] = headers[i];
                    }
                    catch (Exception)
                    {

                    }
                }
                rowList.Insert(0, headerRow);
            }
            else
            {
                var row = dataTable.NewRow();
                dataTable.Columns.Add("", typeof (string));
                row[0] = "No Data Available.";
                rowList.Add(row);
            }

            return rowList;
        }
        private StreamReader FTPFileDowbloadRequest(string fileName)
        {
            var request = (FtpWebRequest)WebRequest.Create("ftp://ftp.ubmits.com/" + fileName);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            request.Credentials = new NetworkCredential("GESftp", "drUhe6ru");

            var response = (FtpWebResponse)request.GetResponse();

            var responseStream = response.GetResponseStream();
            var reader = new StreamReader(responseStream);

            return reader;
        }

        private StreamReader FTPListDirectoryRequest()
        {
            var request = (FtpWebRequest)WebRequest.Create("ftp://ftp.ubmits.com");
            request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;

            request.Credentials = new NetworkCredential("GESftp", "drUhe6ru");

            var response = (FtpWebResponse)request.GetResponse();

            var responseStream = response.GetResponseStream();
            var reader = new StreamReader(responseStream);

            return reader;
        }
    }
}
