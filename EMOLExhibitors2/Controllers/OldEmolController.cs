﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EMOLExhibitors2.ViewModels;

namespace EMOLExhibitors2.Controllers
{
    public class OldEmolController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            int? eventId = null;

            if (Session["emol"] == null)
            {
                Session["emol"] = "old";
                return View(CreateViewModel(eventId));
            }

            if (Session["emol"].ToString() == "old")
            {
                if (Session["EventId"] != null)
                {
                    ViewBag.SelectedEvent = Session["EventId"];
                    eventId = (int) Session["EventId"];
                }
            }
            else Session["emol"] = "old";

            return View(CreateViewModel(eventId));
        }
        [Authorize]
        [HttpPost]
        public ActionResult Index(FormCollection formCollection)
        {
            if (formCollection["Event"] != null && formCollection["Event"] != String.Empty)
            {
                var eventId = int.Parse(formCollection["Event"]);

                Session["EventId"] = eventId;
                ViewBag.SelectedEvent = eventId.ToString();

                return View(CreateViewModel(eventId));
            }
            return View(CreateViewModel(null));
        }
        [Authorize]
        public ActionResult DownloadCSV()
        {
            //var spreadsheet = new SpreadsheetCreator();

            var viewModel = new OldEmolIndexViewModel();

            OldEmolDBContext _context = new OldEmolDBContext();

            var events = _context.Events.Where(e => e.CloseDate >= DateTime.Now).OrderBy(e => e.Name).ToList();
            var eventId = Session["EventId"];
            List<OldEmolExhibitorDetails> exhibitors;

            var current = System.Web.HttpContext.Current;
            current.Response.Clear();

            current.Response.ContentType = "text/csv";
            current.Response.ContentEncoding = System.Text.Encoding.Unicode;
            current.Response.AppendHeader("Content-Disposition", "attachment; filename=EMOL_exhibitors.csv");

            if (eventId != null)
            {
                var idParam = new SqlParameter
                {
                    ParameterName = "ID",
                    Value = eventId
                };
                exhibitors =
                    _context.Database.SqlQuery<OldEmolExhibitorDetails>("exec GetExhibitors @ID ", idParam)
                            .ToList<OldEmolExhibitorDetails>();

                ViewBag.SelectedEvent = eventId;

                viewModel.Events = events;
                viewModel.ExhibitorDetails = exhibitors;

                var exhibitorProperties = typeof(OldEmolExhibitorDetails).GetProperties();
                
                //int columnCounter = 0;
                foreach (var exhibitorProperty in exhibitorProperties)
                {
                    current.Response.Write(exhibitorProperty.Name + ",");
                    //columnCounter++;
                    //spreadsheet.AddData(1, columnCounter, exhibitorProperty.Name);
                }

                current.Response.Write(Environment.NewLine);

                foreach (var exhibitor in exhibitors)
                {
                    foreach (var property in exhibitor.GetType().GetProperties())
                    {
                        if (property.GetValue(exhibitor, null) != null)
                        {
                            string valueToInsert = property.GetValue(exhibitor, null).ToString();
                            valueToInsert = valueToInsert.Replace(',', ' ');
                            valueToInsert = valueToInsert.Replace("&nbsp;", " ");
                            valueToInsert = valueToInsert.Replace("&amp;", "&");

                            current.Response.Write(valueToInsert + ",");
                        }
                        else
                        {
                            current.Response.Write(String.Empty + ",");
                        }
                    }
                    current.Response.Write(Environment.NewLine);
                }
                
                current.Response.End();

                //int rowCounter = 1;
                //foreach (var exhibitor in exhibitors)
                //{
                //    rowCounter++;
                //    columnCounter = 0;
                //    foreach (var property in exhibitor.GetType().GetProperties())
                //    {
                //        columnCounter++;
                //        if (property.GetValue(exhibitor, null) != null)
                //        {
                //            spreadsheet.AddData(rowCounter, columnCounter, property.GetValue(exhibitor, null).ToString());
                //        }
                //    }
                //}
            }

            return View("Index", viewModel);
        }

        private OldEmolIndexViewModel CreateViewModel(int? eventId)
        {
            var viewModel = new OldEmolIndexViewModel();
            OldEmolDBContext _context = new OldEmolDBContext();

            var events = _context.Events.Where(e => e.CloseDate >= DateTime.Now).OrderBy(e => e.Name).ToList();

            List<OldEmolExhibitorDetails> exhibitors;
            if (eventId != null)
            {
                var idParam = new SqlParameter
                {
                    ParameterName = "ID",
                    Value = eventId
                };
                exhibitors =
                    _context.Database.SqlQuery<OldEmolExhibitorDetails>("exec GetExhibitors @ID ", idParam)
                            .ToList<OldEmolExhibitorDetails>();
                if (exhibitors.Count == 0) ViewBag.Message = "No records found.";
            }
            else
            {
                exhibitors = new List<OldEmolExhibitorDetails>();
                ViewBag.Message = "Please select event.";
            }

            viewModel.Events = events;
            viewModel.ExhibitorDetails = exhibitors;

            return viewModel;
        }

    }
}
