﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Excel = Microsoft.Office.Interop.Excel;

namespace EMOLExhibitors2
{
    public class SpreadsheetCreator
    {
        private Excel.Application app = null;
        private Excel.Workbook workbook = null;
        private Excel.Worksheet worksheet = null;
        private Excel.Range range;

        public SpreadsheetCreator()
        {
            CreateDoc();
        }
        public void CreateDoc()
        {
            try
            {
                app = new Excel.Application();
                app.Visible = true;
                workbook = app.Workbooks.Add(1);
                worksheet = (Excel.Worksheet)workbook.Sheets[1];
            }
            catch (Exception exs)
            {
                
            }
        }

        public void CreateBasicHeaders(int row, int col, string htext)
        {
            worksheet.Cells[row, col] = htext;
        }

        public void AddData(int row, int col, string data)
        {
            worksheet.Cells[row, col] = data;
        }

        public void AddIllegalData(int row, int col, string data)
        {
            worksheet.UsedRange[row, col].Font.Color = System.Drawing.Color.Red;
            worksheet.Cells[row, col] = data;
        }

        public void Save(string fileName)
        {
            var filePath = String.Format("C:\\{0}.xlsx", fileName);
            workbook.SaveAs(filePath);
        }
        public void ReleaseDocument()
        {
            workbook.Close(true, null, null);
            app.Quit();
            System.Runtime.InteropServices.Marshal.ReleaseComObject(worksheet);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(workbook);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
        }
        public Excel.Range ReadFile(string filePath)
        {
            var fileExtension = Path.GetExtension(filePath);

            if (fileExtension == ".txt")
            {
                workbook = app.Workbooks.Add();
                worksheet = workbook.ActiveSheet;

                var reader = new StreamReader(System.IO.File.OpenRead(filePath));
                var lineCounter = 0;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    for (int j = 0; j < values.Length; j++)
                    {
                        worksheet.Cells[lineCounter + 1, j + 1] = values[j];
                    }
                    lineCounter++;
                }
                range = worksheet.UsedRange;
            }
            else if (fileExtension == ".xlsx" || fileExtension == ".csv")
            {
                workbook = app.Workbooks.Open(filePath);
                worksheet = (Excel.Worksheet)workbook.Worksheets.get_Item(1);
                range = worksheet.UsedRange;
            }
            return range;
        }
    }
}