﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;

namespace EMOLExhibitors2.ViewModels
{
    public class UBMIndexViewModel
    {
        // Display Attribute will appear in the Html.LabelFor

        [Display(Name = "UBM Files")]
        public string SelectedFile { get; set; }

        public IEnumerable<UBMFile> Files { get; set; }
        public List<DataRow> Data { get; set; }
    }
}