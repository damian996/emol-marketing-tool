﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMOLExhibitors2.ViewModels
{
    public class OldEmolIndexViewModel
    {
        public IEnumerable<OldEmolEvent> Events { get; set; }
        public IEnumerable<OldEmolExhibitorDetails> ExhibitorDetails { get; set; }
    }
}