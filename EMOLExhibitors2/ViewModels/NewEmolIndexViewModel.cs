﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMOLExhibitors2.ViewModels
{
    public class NewEmolIndexViewModel
    {
        public IEnumerable<NewEmolEvent> Events { get; set; }
        public IEnumerable<NewEmolExhibitorDetails> ExhibitorDetails { get; set; }
    }
}