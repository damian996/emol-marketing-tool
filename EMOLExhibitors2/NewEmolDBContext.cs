﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Data.Entity;
using EMOLExhibitors2;

namespace EMOLExhibitors2
{
    public class NewEmolDBContext : DbContext
    {
         public NewEmolDBContext()
            : base(ConfigurationManager.ConnectionStrings["EMOL_LIVEEntities1"].ConnectionString)
        {
        }
        public DbSet<NewEmolEvent> Events { get; set; }
    }
}