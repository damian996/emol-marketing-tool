﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EMOLExhibitors2
{
    public class OldEmolDBContext : DbContext
    {
        public OldEmolDBContext()
            : base(ConfigurationManager.ConnectionStrings["EMOL_MesCoreEntities"].ConnectionString)
        {
        }
        public DbSet<OldEmolEvent> Events { get; set; }
    }
}